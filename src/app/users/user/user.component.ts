import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: {id: number, name: string};
  paramsSubscription: Subscription;

  constructor(private route: ActivatedRoute) { }
  // the snapshot at the beginning could be removed, since the subscribe
  // does a better work, but is left here as example
  ngOnInit() {
    // get snapshot of route onInit
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };
    // use observable for further changes
    // subscribe takes 3 functions as arguments
    // 1: whenever new data is sent through the observable, in short, whenever the params change
    // 2: for errorhandling
    // 3: when its completed
    // paramsSubscription is bound to the property, so that it can be destroyed
    // when the component is not used and thus removed from the memory
    // angular handles it automatically but as an example is used here
    this.paramsSubscription = this.route.params
      .subscribe(
        (params: Params) => {
          this.user.id = params['id'];
          this.user.name = params['name'];
        }
      );
  }
  // you dont have to do this, because angular handles it, but is important to know
  // and you should do it if you create your own observables
  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ServersService } from './servers.service';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  private servers: {id: number, name: string, status: string}[] = [];

  constructor(
    private serversService: ServersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.servers = this.serversService.getServers();
  }
  // A path is relative without a slash at the beginning, and absolute with a slash
  // navigate navigates to the path, also useful ./servers, ../servers
  // relativeTo with ActivatedRoute, gets the current route and resolves it with
  // the path that needs to be navigated, for example:
  // if you use 'servers' as relative path, and the current route is localhost/servers,
  // then it will resolve to localhost/servers/servers, which will end in a bug
  reload() {
    // this.router.navigate(['servers'], {relativeTo: this.route});
  }
}
